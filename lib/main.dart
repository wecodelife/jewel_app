// main.dart
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/ui/screens/home_page.dart';
import 'package:jewel_app/src/ui/screens/login.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart' as path_provider;


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  // final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  // Hive.init(appDocDir.path);
  Hive.initFlutter();
  await Hive.openBox('app');
  runApp(MyApp());
}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(color: Colors.black,
      title: 'Tele-Jewel',debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.black,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Hive.box('app').get(1) !=null? HomePage(token: Hive.box('app').get(1),) : LoginPage(),
    );
  }
}
