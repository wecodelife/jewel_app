// To parse this JSON data, do
//
//     final selectAnswerRequestModel = selectAnswerRequestModelFromJson(jsonString);

import 'dart:convert';

SelectAnswerRequestModel selectAnswerRequestModelFromJson(String str) => SelectAnswerRequestModel.fromJson(json.decode(str));

String selectAnswerRequestModelToJson(SelectAnswerRequestModel data) => json.encode(data.toJson());

class SelectAnswerRequestModel {
  SelectAnswerRequestModel({
    this.question,
    this.option,
    this.customer,
  });

  int question;
  int option;
  int customer;

  factory SelectAnswerRequestModel.fromJson(Map<String, dynamic> json) => SelectAnswerRequestModel(
    question: json["question"] == null ? null : json["question"],
    option: json["option"] == null ? null : json["option"],
    customer: json["customer"] == null ? null : json["customer"],
  );

  Map<String, dynamic> toJson() => {
    "question": question == null ? null : question,
    "option": option == null ? null : option,
    "customer": customer == null ? null : customer,
  };
}
