// To parse this JSON data, do
//
//     final customerResponseModel = customerResponseModelFromJson(jsonString);

import 'dart:convert';

CustomerResponseModel customerResponseModelFromJson(String str) => CustomerResponseModel.fromJson(json.decode(str));

String customerResponseModelToJson(CustomerResponseModel data) => json.encode(data.toJson());

class CustomerResponseModel {
  CustomerResponseModel({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final Data data;

  factory CustomerResponseModel.fromJson(Map<String, dynamic> json) => CustomerResponseModel(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.name,
    this.group,
    this.area,
    this.areaName,
    this.groupName,
    this.address,
    this.email,
    this.isAttended,
    this.isAssigned,
    this.number,
    this.remarks,
  });

  final int id;
  final String name;
  final int group;
  final int area;
  final String areaName;
  final String groupName;
  final String address;
  final String email;
  final bool isAttended;
  final bool isAssigned;
  final List<Number> number;
  final List<Number> remarks;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    group: json["group"] == null ? null : json["group"],
    area: json["area"] == null ? null : json["area"],
    areaName: json["area_name"] == null ? null : json["area_name"],
    groupName: json["group_name"] == null ? null : json["group_name"],
    address: json["address"] == null ? null : json["address"],
    email: json["email"] == null ? null : json["email"],
    isAttended: json["is_attended"] == null ? null : json["is_attended"],
    isAssigned: json["is_assigned"] == null ? null : json["is_assigned"],
    number: json["number"] == null ? null : List<Number>.from(json["number"].map((x) => Number.fromJson(x))),
    remarks: json["remarks"] == null ? null : List<Number>.from(json["remarks"].map((x) => Number.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "group": group == null ? null : group,
    "area": area == null ? null : area,
    "area_name": areaName == null ? null : areaName,
    "group_name": groupName == null ? null : groupName,
    "address": address == null ? null : address,
    "email": email == null ? null : email,
    "is_attended": isAttended == null ? null : isAttended,
    "is_assigned": isAssigned == null ? null : isAssigned,
    "number": number == null ? null : List<dynamic>.from(number.map((x) => x.toJson())),
    "remarks": remarks == null ? null : List<dynamic>.from(remarks.map((x) => x.toJson())),
  };
}

class Number {
  Number({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.phoneNumber,
    this.agent,
    this.status,
    this.remarks,
    this.date,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int phoneNumber;
  final int agent;
  final int status;
  final String remarks;
  final DateTime date;

  factory Number.fromJson(Map<String, dynamic> json) => Number(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    agent: json["agent"] == null ? null : json["agent"],
    status: json["status"] == null ? null : json["status"],
    remarks: json["remarks"] == null ? null : json["remarks"],
    date: json["date"] == null ? null : DateTime.parse(json["date"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "agent": agent == null ? null : agent,
    "status": status == null ? null : status,
    "remarks": remarks == null ? null : remarks,
    "date": date == null ? null : date.toIso8601String(),
  };
}
