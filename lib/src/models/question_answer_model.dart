// To parse this JSON data, do
//
//     final questionAnswerModel = questionAnswerModelFromJson(jsonString);

import 'dart:convert';

QuestionAnswerModel questionAnswerModelFromJson(String str) => QuestionAnswerModel.fromJson(json.decode(str));

String questionAnswerModelToJson(QuestionAnswerModel data) => json.encode(data.toJson());

class QuestionAnswerModel {
  QuestionAnswerModel({
    this.message,
    this.status,
    this.data,
  });

  String message;
  int status;
  List<Datum> data;

  factory QuestionAnswerModel.fromJson(Map<String, dynamic> json) => QuestionAnswerModel(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.content,
    this.options,
  });

  int id;
  String content;
  List<Option> options;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    content: json["content"] == null ? null : json["content"],
    options: json["options"] == null ? null : List<Option>.from(json["options"].map((x) => Option.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "content": content == null ? null : content,
    "options": options == null ? null : List<dynamic>.from(options.map((x) => x.toJson())),
  };
}

class Option {
  Option({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.isAnswer,
    this.question,
  });

  int id;
  bool isActive;
  bool isDeleted;
  DateTime createdAt;
  DateTime updatedAt;
  String name;
  bool isAnswer;
  int question;

  factory Option.fromJson(Map<String, dynamic> json) => Option(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    name: json["name"] == null ? null : json["name"],
    isAnswer: json["is_answer"] == null ? null : json["is_answer"],
    question: json["question"] == null ? null : json["question"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "name": name == null ? null : name,
    "is_answer": isAnswer == null ? null : isAnswer,
    "question": question == null ? null : question,
  };
}
