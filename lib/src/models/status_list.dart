// To parse this JSON data, do
//
//     final statusListResponseModel = statusListResponseModelFromJson(jsonString);

import 'dart:convert';

StatusListResponseModel statusListResponseModelFromJson(String str) => StatusListResponseModel.fromJson(json.decode(str));

String statusListResponseModelToJson(StatusListResponseModel data) => json.encode(data.toJson());

class StatusListResponseModel {
  StatusListResponseModel({
    this.message,
    this.status,
    this.data,
  });

  String message;
  int status;
  List<Datum> data;

  factory StatusListResponseModel.fromJson(Map<String, dynamic> json) => StatusListResponseModel(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.description,
    this.company,
  });

  int id;
  bool isActive;
  bool isDeleted;
  DateTime createdAt;
  DateTime updatedAt;
  String name;
  dynamic description;
  int company;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    name: json["name"] == null ? null : json["name"],
    description: json["description"],
    company: json["company"] == null ? null : json["company"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "name": name == null ? null : name,
    "description": description,
    "company": company == null ? null : company,
  };
}
