// To parse this JSON data, do
//
//     final updatePhoneStatusRequestModel = updatePhoneStatusRequestModelFromJson(jsonString);

import 'dart:convert';

UpdatePhoneStatusRequestModel updatePhoneStatusRequestModelFromJson(String str) => UpdatePhoneStatusRequestModel.fromJson(json.decode(str));

String updatePhoneStatusRequestModelToJson(UpdatePhoneStatusRequestModel data) => json.encode(data.toJson());

class UpdatePhoneStatusRequestModel {
  UpdatePhoneStatusRequestModel({
    this.phoneNumber,
    this.status,
  });

  final int phoneNumber;
  final int status;

  factory UpdatePhoneStatusRequestModel.fromJson(Map<String, dynamic> json) => UpdatePhoneStatusRequestModel(
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "status": status == null ? null : status,
  };
}
