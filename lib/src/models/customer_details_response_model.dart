// To parse this JSON data, do
//
//     final customerDetailsResponseModel = customerDetailsResponseModelFromJson(jsonString);

import 'dart:convert';

CustomerDetailsResponseModel customerDetailsResponseModelFromJson(String str) => CustomerDetailsResponseModel.fromJson(json.decode(str));

String customerDetailsResponseModelToJson(CustomerDetailsResponseModel data) => json.encode(data.toJson());

class CustomerDetailsResponseModel {
  CustomerDetailsResponseModel({
    this.message,
    this.status,
    this.data,
  });

  String message;
  int status;
  Data data;

  factory CustomerDetailsResponseModel.fromJson(Map<String, dynamic> json) => CustomerDetailsResponseModel(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.name,
    this.group,
    this.groupName,
    this.address,
    this.phoneNumber,
    this.phoneRes,
    this.mobileNumber,
    this.email,
    this.isAttended,
  });

  int id;
  String name;
  int group;
  String groupName;
  String address;
  String phoneNumber;
  String phoneRes;
  String mobileNumber;
  dynamic email;
  bool isAttended;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    group: json["group"] == null ? null : json["group"],
    groupName: json["group_name"] == null ? null : json["group_name"],
    address: json["address"] == null ? null : json["address"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    phoneRes: json["phone_res"] == null ? null : json["phone_res"],
    mobileNumber: json["mobile_number"] == null ? null : json["mobile_number"],
    email: json["email"],
    isAttended: json["is_attended"] == null ? null : json["is_attended"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "group": group == null ? null : group,
    "group_name": groupName == null ? null : groupName,
    "address": address == null ? null : address,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "phone_res": phoneRes == null ? null : phoneRes,
    "mobile_number": mobileNumber == null ? null : mobileNumber,
    "email": email,
    "is_attended": isAttended == null ? null : isAttended,
  };
}
