// To parse this JSON data, do
//
//     final questionAnswerRequest = questionAnswerRequestFromJson(jsonString);

import 'dart:convert';

QuestionAnswerRequest questionAnswerRequestFromJson(String str) => QuestionAnswerRequest.fromJson(json.decode(str));

String questionAnswerRequestToJson(QuestionAnswerRequest data) => json.encode(data.toJson());

class QuestionAnswerRequest {
  QuestionAnswerRequest({
    this.answer,
  });

  List<Answer> answer;

  factory QuestionAnswerRequest.fromJson(Map<String, dynamic> json) => QuestionAnswerRequest(
    answer: json["answer"] == null ? null : List<Answer>.from(json["answer"].map((x) => Answer.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "answer": answer == null ? null : List<dynamic>.from(answer.map((x) => x.toJson())),
  };
}

class Answer {
  Answer({
    this.question,
    this.option,
    this.customer,
  });

  String question;
  String option;
  String customer;

  factory Answer.fromJson(Map<String, dynamic> json) => Answer(
    question: json["question"] == null ? null : json["question"],
    option: json["option"] == null ? null : json["option"],
    customer: json["customer"] == null ? null : json["customer"],
  );

  Map<String, dynamic> toJson() => {
    "question": question == null ? null : question,
    "option": option == null ? null : option,
    "customer": customer == null ? null : customer,
  };
}
