// To parse this JSON data, do
//
//     final phoneResponseModel = phoneResponseModelFromJson(jsonString);

import 'dart:convert';

PhoneResponseModel phoneResponseModelFromJson(String str) => PhoneResponseModel.fromJson(json.decode(str));

String phoneResponseModelToJson(PhoneResponseModel data) => json.encode(data.toJson());

class PhoneResponseModel {
  PhoneResponseModel({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.phoneNumber,
    this.agent,
    this.status,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int phoneNumber;
  final int agent;
  final int status;

  factory PhoneResponseModel.fromJson(Map<String, dynamic> json) => PhoneResponseModel(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    agent: json["agent"] == null ? null : json["agent"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "agent": agent == null ? null : agent,
    "status": status == null ? null : status,
  };
}
