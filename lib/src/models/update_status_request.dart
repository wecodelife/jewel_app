// To parse this JSON data, do
//
//     final updateStatusRequest = updateStatusRequestFromJson(jsonString);

import 'dart:convert';

UpdateStatusRequest updateStatusRequestFromJson(String str) => UpdateStatusRequest.fromJson(json.decode(str));

String updateStatusRequestToJson(UpdateStatusRequest data) => json.encode(data.toJson());

class UpdateStatusRequest {
  UpdateStatusRequest({
    this.customer,
    this.status,
  });

  String customer;
  String status;

  factory UpdateStatusRequest.fromJson(Map<String, dynamic> json) => UpdateStatusRequest(
    customer: json["customer"] == null ? null : json["customer"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "customer": customer == null ? null : customer,
    "status": status == null ? null : status,
  };
}
