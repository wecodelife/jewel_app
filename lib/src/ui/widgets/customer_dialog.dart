import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/ui/widgets/customer_card.dart';
import 'package:jewel_app/src/ui/widgets/raised_button.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class CustomerDialog extends StatefulWidget {
  @override
  _CustomerDialogState createState() => _CustomerDialogState();
}

class _CustomerDialogState extends State<CustomerDialog> {
  bool isLoading = false;
  String statusCode;
  String message;
  String bride;
  String guardian;
  String house;
  String place;
  String district;
  String remark;
  String postOffice;
  String phone1;
  String phone2;
  String phone3;
  // List<String> phoneNumbers=[];
  // String bride;
  TextEditingController brideController = TextEditingController();
  TextEditingController guardianController = TextEditingController();
  TextEditingController houseController = TextEditingController();
  TextEditingController placeController = TextEditingController();
  TextEditingController districtController = TextEditingController();
  TextEditingController postOfficeController = TextEditingController();
  TextEditingController remarkController = TextEditingController();
  TextEditingController phone1Controller = TextEditingController();
  TextEditingController phone2Controller = TextEditingController();
  TextEditingController phone3Controller = TextEditingController();
  void _showDialog1() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
                side: BorderSide(color: Colors.red, width: 0)),
            content: CustomerDialog(),
            actions: <Widget>[],
          ),
        );
      },
    );
  }

  void addCustomer() async {
    var client = http.Client();
    // phoneNumbers.add(phone1.toString());
    // phoneNumbers.add(phone2.toString());
    // phoneNumbers.add(phone3.toString());
    setState(() {
      isLoading = true;
    });
    try {
      print(Hive.box('app').get(3).toString());
      print(bride);
      print(guardian);
      print(house);
      print(place);
      print(phone1);
      var res = await client.post(Urls.addCustomer, headers: {
        "Authorization": "Token " + Hive.box('app').get(1)
      }, body: {
        "bride_name": bride,
        "name_of_guardian": guardian,
        "house_name": house,
        "place": place,
        "post_office": postOffice,
        "district": district,
        "remarks": remark !=null ? remark : '',
        "agent": Hive.box('app').get(3).toString(),
        "phone_number1": phone1 !=null ? phone1 : '',
        "phone_number2":phone2 !=null ? phone2 : '',
        "phone_number3":phone3 !=null ? phone3 : '',
      });
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      statusCode = jsonResponse['status'].toString();
      message = jsonResponse['message'];
      if (statusCode == 200.toString() || statusCode == 201.toString()) {
        showToast(message);
      } else {
        showToast("Please Check your connection!!");
      }
      print(jsonResponse["data"]);
    } finally {
      client.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Add Customer'),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CustomerCard(
                  label: 'Name of Bride',
                  autoFocus: true,
                  textEditingController: brideController,
                  onChange: (value) {
                    value = brideController.text.toString();
                    bride = value;
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                CustomerCard(
                  label: 'Name of Guardian',
                  textEditingController: guardianController,
                  onChange: (value) {
                    value = guardianController.text.toString();
                    guardian = value;
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                CustomerCard(
                  label: 'House Name',
                  textEditingController: houseController,
                  onChange: (value) {
                    value = houseController.text.toString();
                    house = value;
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                CustomerCard(
                  label: 'Place',
                  textEditingController: placeController,
                  onChange: (value) {
                    value = placeController.text.toString();
                    place = value;
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                CustomerCard(
                  label: 'District',
                  textEditingController: districtController,
                  onChange: (value) {
                    value = districtController.text.toString();
                    district = value;
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                CustomerCard(
                  label: 'Post Office',
                  textEditingController: postOfficeController,
                  onChange: (value) {
                    value = postOfficeController.text.toString();
                    postOffice = value;
                  },
                ),
              ],
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 60),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Card(
                    child: Container(
                  height: screenHeight(context, dividedBy: 4),
                  width: screenWidth(context, dividedBy: 4),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 90)),
                    child: TextField(
                      style: TextStyle(
                          fontSize: screenHeight(context, dividedBy: 50)),
                      controller: remarkController,
                      onChanged: (value) {
                        value = remarkController.text.toString();
                        remark = value;
                      },
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Remarks',
                          hintStyle: TextStyle(
                              fontSize: screenHeight(context, dividedBy: 50))),
                    ),
                  ),
                )),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                CustomerCard(
                  label: 'Phone Number',
                  textEditingController: phone1Controller,
                  onChange: (value) {
                    phone1 = value;
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                CustomerCard(
                  label: 'Phone Number',
                  textEditingController: phone2Controller,
                  onChange: (value) {
                    phone2 = value;

                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                CustomerCard(
                  label: 'Phone Number',
                  textEditingController: phone3Controller,
                  onChange: (value) {
                    phone3 = value;

                  },
                ),
              ],
            )
          ],
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              BuildRaisedButton(
                backgroundColor: Constants.kitGradients[2],
                buttonName: "Save",
                width: screenWidth(context, dividedBy: 12),
                height: screenWidth(context, dividedBy: 50),
                onPressed: () {
                  print(bride);
                  print(guardian);
                  print(house);
                  print(place);
                  print(district);
                  print(postOffice);
                  print(remark);
                  print(phone1);
                  print(phone2);
                  print(phone3);
                  addCustomer();
                  Navigator.pop(context);
                },
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 100),
              ),
              BuildRaisedButton(
                backgroundColor: Constants.kitGradients[2],
                buttonName: "Save & Add Another",
                width: screenWidth(context, dividedBy: 8),
                height: screenWidth(context, dividedBy: 50),
                onPressed: () {
                  print(bride);
                  print(guardian);
                  print(house);
                  print(place);
                  print(district);
                  print(postOffice);
                  print(remark);
                  print(phone1);
                  print(phone2);
                  print(phone3);
                  addCustomer();
                  Navigator.pop(context);
                  _showDialog1();
                },
              )
            ],
          ),
        )
      ],
    );
  }
}
