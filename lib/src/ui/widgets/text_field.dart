import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';

class BuildTextField extends StatefulWidget {
  final String label;
  final String value;
  BuildTextField({
    this.label,
    this.value
});
  @override
  _BuildTextFieldState createState() => _BuildTextFieldState();
}

class _BuildTextFieldState extends State<BuildTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child:Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: screenWidth(context,dividedBy: 8),
              child: Text(widget.label,
              style: TextStyle(fontSize: screenWidth(context,dividedBy: 90),color: Colors.black),),
            ),

          ),
          Padding(
            padding:  EdgeInsets.all(8.0),
            child: Container(
              child: Text(widget.value,
              style: TextStyle(fontSize: screenWidth(context,dividedBy: 70),color: Colors.black,fontWeight: FontWeight.w500),),
            ),
          ),

        ],
      )
    );
  }
}
