import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import 'package:jewel_app/src/models/status_list.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class StatusDrop extends StatefulWidget {
  int statusDropId;
  List<int> statusId;
  final ValueChanged<int> getStatusDropId;
  StatusDrop({this.statusDropId,this.getStatusDropId,this.statusId});
  @override
  _StatusDropState createState() => _StatusDropState();
}

class _StatusDropState extends State<StatusDrop> {
  String _value = '';
  bool isLoading = false;
  String statusCode;
  String message;
  String phoneStatusValue="Status";
  List<String> statusNames = [
    // 'attended',
    // 'not attended'
  ];
  List<int> statusIds = [];
  StatusListResponseModel responseModel = new StatusListResponseModel();
  int selectedStatusId;

  @override
  void initState() {
    print(widget.statusDropId.toString() +"AAAAA");
    setState(() {
      isLoading = true;
    });
    getStatusId();
    getStatusList();
    widget.getStatusDropId(widget.statusDropId);
    Hive.box('app').put('status', statusIds.indexOf(widget.statusDropId));
    // print("ress"+responseModel.data.toString());
    super.initState();
  }

  void getStatusId() {
    setState(() {
      selectedStatusId = statusIds.indexOf(widget.statusDropId);
    });
    // selectedStatusId = widget.statusDropId;
    Hive.box("app").put(4, selectedStatusId);
    print(selectedStatusId);
  }

  void getStatusList() async {
    var client = http.Client();
    try {
      var res = await client.get(
        Urls.phoneNumUrl,
          headers: {"Authorization": "Token " + Hive.box('app').get(1)}
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      // statusCode = jsonResponse['status'];
      message = jsonResponse['message'];
      print("messs" + jsonResponse.toString());
      // statusNames=jsonResponse['data']["name"];
      print(jsonResponse["data"]);
      await setState(() {
        responseModel =
            StatusListResponseModel.fromJson(convert.jsonDecode(res.body));
      });

      for (int i = 0; i < responseModel.data.length; i++) {
        statusNames.add(responseModel.data[i].name);
        statusIds.add(responseModel.data[i].id);
      }

      // print("sas"+responseModel.data[0].toString());
    } finally {
      client.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    // return isLoading==false? Row(
    return isLoading == false
        ? Card(
            shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.circular(screenHeight(context, dividedBy: 40)),
                side: BorderSide(color: Colors.red, width: 0)),
            color: Colors.grey.withOpacity(.50),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 100),
                  ),
                  DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      value: _value,
                      hint: Text(
                        statusNames[statusIds.indexOf(widget.statusDropId)],
                        style: TextStyle(
                            fontSize: screenWidth(context, dividedBy: 90),
                            color: Colors.black),
                      ),
                      items: statusNames.map((String value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(
                            value,
                            style: TextStyle(
                                fontSize: screenWidth(context, dividedBy: 90),
                                color: Colors.black),
                          ),
                        );
                      }).toList(),
                      onChanged: (val) {
                        setState(() {
                          _value = val;
                          Hive.box('app').put(4, statusIds[statusNames.indexOf(_value)]);
                        });

                        // updateStatus();
                        // print(widget.statusDropId);
                        // print(phoneStatusValue);
                        widget.getStatusDropId(statusIds[statusNames.indexOf(_value)]);
                        // getStatusId();
                      },
                    ),
                  ),
                ],
              ),
            ),
          )
        : Container();
  }
}
