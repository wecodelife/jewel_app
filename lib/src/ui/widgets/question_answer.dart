import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/models/answer_request_model.dart';
import 'package:jewel_app/src/models/question_answer_model.dart';
import 'package:jewel_app/src/models/question_answer_request_model.dart';
import 'package:jewel_app/src/ui/widgets/options.dart';
import 'package:jewel_app/src/ui/widgets/others_field.dart';
import 'package:jewel_app/src/ui/widgets/select_deselect_icon.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class QuestionAnswer extends StatefulWidget {
  @override
  _QuestionAnswerState createState() => _QuestionAnswerState();
}

class _QuestionAnswerState extends State<QuestionAnswer> {
  List<List<bool>> isSelected = [[]];
  int selectedParentIndex;
  int selectedChildIndex;
  bool isLoading = false;
  bool selection;
  QuestionAnswerModel responseModel = new QuestionAnswerModel();
  QuestionAnswerRequest questionAnswerRequestModel ;
  var box = Hive.box('app');
  List<int> optionValue=[];
  int count = 0;
  List<String> selectedOptions = [];


  @override
  void initState() {
    setState(() {
      isLoading = true;
    });
    getQuestions();
    super.initState();
  }

  void getQuestions() async {
    await Hive.openBox('app');
    var client = http.Client();
    var box = Hive.box('app');

    try {
      var res = await client.get(
        Urls.questionAnswerUrl,
        headers: {"Authorization": "Token " + box.get(1)},
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      print(jsonResponse["data"]);
      if (jsonResponse['message'] != "No data found")
        setState(() {
          responseModel =
              QuestionAnswerModel.fromJson(convert.jsonDecode(res.body));
        });
      print(responseModel.data);
    } finally {
      client.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    return isLoading == false
        ? Expanded(
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Text("Questions",style: TextStyle(fontSize: screenWidth(context,dividedBy: 40)),),
                ),

                new ListView.builder(
                    shrinkWrap: true,

                    scrollDirection: Axis.vertical,
                    itemCount: responseModel.data.length,
                    itemBuilder: (BuildContext ctxt, int indexParent) {
                      isSelected.add([]);
                      return Container(
                          child: Padding(
                        padding:
                            EdgeInsets.all(screenWidth(context, dividedBy: 200)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              children: [
                                new Text(
                                  "${indexParent + 1}.",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize:
                                          screenWidth(context, dividedBy: 100),
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 50),
                                ),
                                Container(
                                  width: screenWidth(context, dividedBy: 4),
                                  child: new Text(
                                    responseModel.data[indexParent].content,
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                        color: Colors.black,
                                        height: 1,
                                        fontSize:
                                            screenWidth(context, dividedBy: 85),
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            ListView.builder(
                                shrinkWrap: true,
                                itemCount:
                                    responseModel.data[indexParent].options.length,
                                scrollDirection: Axis.vertical,
                                physics: NeverScrollableScrollPhysics(),
                                itemBuilder: (BuildContext ctxt, int index) {
                                  isSelected[indexParent].add(false);
                                  optionValue.add(-1);
                                  return Options(
                                    selected: isSelected[indexParent][index],
                                    optionContent: responseModel
                                        .data[indexParent].options[index].name,
                                    optionId: responseModel
                                        .data[indexParent].options[index].id,
                                    parentIndex: indexParent,
                                    questionId: responseModel.data[indexParent].id,
                                    index: index,
                                    getOptionVal: (val) {
                                      optionValue[indexParent] = val;
                                      // questionAnswerRequestModel.answer[indexParent]=(Answer(
                                      //     customer: box.get(3).toString(),
                                      //     question: responseModel
                                      //         .data[indexParent].id
                                      //         .toString(),
                                      //     option: optionValue.toString()));
                                      // print(questionAnswerRequestModel.answer[0].option);
                                    },
                                    getParentIndex: (parentIndex) {
                                      selectedParentIndex = parentIndex;
                                    },
                                    selectFunction: (index) {
                                      for (int i = 0;
                                          i <
                                              isSelected[selectedParentIndex]
                                                  .length;
                                          i++) {
                                        if (i != index) {
                                          setState(() {
                                            isSelected[selectedParentIndex][i] =
                                                false;
                                            Hive.box('app').delete('questionId'+responseModel.data[indexParent].id.toString());
                                          });
                                        }
                                      }
                                      if (isSelected[selectedParentIndex][index] ==
                                          false)
                                        setState(() {
                                          isSelected[selectedParentIndex][index] =
                                              true;
                                          Hive.box('app').put('questionId'+responseModel.data[indexParent].id.toString(),'success');
                                        });
                                      else if (isSelected[selectedParentIndex]
                                              [index] ==
                                          true)
                                        setState(() {
                                          count = count - 1;
                                          isSelected[selectedParentIndex][index] =
                                              false;
                                          Hive.box('app').delete('questionId'+responseModel.data[indexParent].id.toString());
                                        });
                                      // setState(() {
                                      //   isSelected[selectedParentIndex][index] =
                                      //   true;
                                      // });
                                      // print(questionAnswerRequestModel
                                      //     .answer[0].option);
                                    },
                                  );
                                }),
                            OthersField(
                              questionId: responseModel.data[indexParent].id,
                            )
                            // Row(
                            //   children: [
                            //     BuildSelectIcon(
                            //       selected: selected,
                            //     ),
                            //     Container(
                            //       height: screenHeight(context,dividedBy: 40),
                            //       width: screenWidth(context,dividedBy: 8),
                            //       child: TextField(
                            //         decoration: InputDecoration(
                            //           border: InputBorder.none,
                            //           hintText: 'Others',
                            //               hintStyle:TextStyle(fontSize: 10) ,
                            //         ),
                            //       ),
                            //       decoration: BoxDecoration(border:Border.all(color: Colors.grey),
                            //
                            //         borderRadius: BorderRadius.only(
                            //             topLeft: Radius.circular(50),
                            //             bottomLeft: Radius.circular(50),
                            //             topRight: Radius.circular(50),
                            //             bottomRight: Radius.circular(50)
                            //         ),
                            //       ),
                            //     ),
                            //   ],
                            // ),
                          ],
                        ),
                      ));
                    }),
              ],
            ),
          )
        : Container();
  }
}
