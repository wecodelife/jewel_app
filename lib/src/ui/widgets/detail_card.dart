// import 'package:flutter/material.dart';
// import 'package:hive/hive.dart';
// import 'package:jewel_app/src/models/customer_details_response_model.dart';
// import 'package:jewel_app/src/models/update_status_request.dart';
// import 'package:jewel_app/src/ui/screens/success_page.dart';
// import 'package:jewel_app/src/ui/widgets/raised_button.dart';
// import 'package:jewel_app/src/ui/widgets/status_dropdown.dart';
// import 'package:jewel_app/src/utils/urls.dart';
// import 'package:jewel_app/src/utils/utils.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert' as convert;
//
// class BuildDetailCard extends StatefulWidget {
//   final CustomerDetailsResponseModel responseModel;
//   BuildDetailCard({this.responseModel});
//   @override
//   _BuildDetailCardState createState() => _BuildDetailCardState();
// }
//
// class _BuildDetailCardState extends State<BuildDetailCard> {
//   var _value = "STATUS";
//   var box = Hive.box('app');
//   bool isLoading = false;
//   String message;
//   String statusCode;
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   void updateStatus() async {
//     await Hive.openBox('app');
//     print(box.get(3).toString());
//     print(box.get(4).toString());
//     var client = http.Client();
//     setState(() {
//       isLoading = true;
//     });
//     try {
//       var res = await client.post(Urls.updateStatusUrl, headers: {
//         "Authorization": "Token " + box.get(1)
//       }, body: {
//         "customer": box.get(3).toString(),
//         "status": box.get(4).toString()
//       });
//       setState(() {
//         isLoading = false;
//       });
//       var jsonResponse = convert.jsonDecode(res.body);
//       statusCode = jsonResponse['status'].toString();
//       message = jsonResponse['message'];
//       if (statusCode == 200.toString() ||statusCode == 201.toString()) {
//         showToast(message);
//         box.put(3, "");
//         box.put(4, "");
//         Navigator.pushAndRemoveUntil(
//           context,
//           MaterialPageRoute(builder: (context) => SuccessPage()),
//           (Route<dynamic> route) => false,
//         );
//       } else {
//         showToast("Please Check your connection!!");
//       }
//       print(jsonResponse["data"]);
//     } finally {
//       client.close();
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     var screenSize = MediaQuery.of(context).size;
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.start,
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         SizedBox(
//           height: screenSize.height / 50,
//         ),
//         Row(
//           mainAxisAlignment: MainAxisAlignment.start,
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             SizedBox(
//               width: (screenSize.width / 20),
//             ),
//             Column(
//               children: [
//                 SizedBox(
//                   height: screenSize.height / 20,
//                 ),
//                 Card(
//                   elevation: 6,
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(26),
//                       side: BorderSide(color: Colors.blue, width: 0)),
//                   child: Container(
//                     width: screenWidth(context, dividedBy: 2),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Container(
//                           width: screenWidth(context, dividedBy: 2),
//                           child: Center(
//                             child: Padding(
//                               padding: const EdgeInsets.all(20.0),
//                               child: Text(
//                                 widget.responseModel.data.name == null
//                                     ? ""
//                                     : widget.responseModel.data.name,
//                                 style: TextStyle(
//                                     fontWeight: FontWeight.bold,
//                                     fontSize: screenSize.width / 50,
//                                     color: Colors.blue),
//                               ),
//                             ),
//                           ),
//                         ),
//                         Container(
//                           height: screenHeight(context, dividedBy: 10),
//                           width: screenWidth(context, dividedBy: 2),
//                           color: Colors.blue,
//                           child: Center(
//                               child: Text(
//                             widget.responseModel.data.phoneNumber != null
//                                 ? widget.responseModel.data.phoneNumber
//                                 : "",
//                             style: TextStyle(
//                                 fontSize: screenSize.width / 30,
//                                 color: Colors.white,
//                                 fontWeight: FontWeight.bold,
//                                 fontStyle: FontStyle.italic),
//                           )),
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Container(
//                               width: screenWidth(context, dividedBy: 3),
//                               child: Padding(
//                                 padding: const EdgeInsets.all(20.0),
//                                 child: Text(
//                                   widget.responseModel.data.address != null
//                                       ? widget.responseModel.data.address
//                                       : "",
//                                   overflow: TextOverflow.visible,
//                                   style: TextStyle(
//                                       color: Colors.grey,
//                                       fontSize: screenSize.width / 100),
//                                 ),
//                               ),
//                             ),
//                             // SizedBox(width: screenSize.width/100,),
//                             Padding(
//                               padding: EdgeInsets.symmetric(
//                                   vertical:
//                                       screenHeight(context, dividedBy: 50)),
//                               child: Container(
//                                 width: screenWidth(context, dividedBy: 10),
//                                 child: Center(
//                                     child: Text(
//                                   widget.responseModel.data.groupName != null
//                                       ? widget.responseModel.data.groupName
//                                       : "",
//                                   style: TextStyle(
//                                       color: Colors.red,
//                                       fontSize: screenSize.width / 70),
//                                 )),
//                               ),
//                             ),
//                           ],
//                         ),
//                         StatusDropDown(),
//                         SizedBox(
//                           height: screenSize.height / 100,
//                         )
//                       ],
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   height: screenSize.height / 20,
//                 ),
//                 BuildRaisedButton(
//                   width: screenWidth(context, dividedBy: 10),
//                   height: screenHeight(context, dividedBy: 20),
//                   backgroundColor: Colors.blue,
//                   buttonName: "SUBMIT",
//                   isLoading: isLoading,
//                   onPressed: () {
//                     if (box.get(4) != "")
//                       updateStatus();
//                     else {
//                       showToast("Please Select Status");
//                     }
//                   },
//                 ),
//               ],
//             )
//           ],
//         ),
//       ],
//     );
//   }
// }
