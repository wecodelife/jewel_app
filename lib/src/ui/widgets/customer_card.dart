import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';

class CustomerCard extends StatefulWidget {
  final String label;
  final TextEditingController textEditingController;
  final Function onChange;
  bool autoFocus;
  CustomerCard({
    this.label,
    this.textEditingController,
    this.onChange,
    this.autoFocus
});

  @override
  _CustomerCardState createState() => _CustomerCardState();
}

class _CustomerCardState extends State<CustomerCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child:Container(
        height: screenHeight(context,dividedBy: 16),
        width: screenWidth(context,dividedBy: 4),
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 100)),

          child: TextField(style: TextStyle(fontSize:screenHeight(context,dividedBy: 50) ,

          ),
            autofocus: widget.autoFocus,
            maxLines: null,
            controller: widget.textEditingController,
            onChanged: widget.onChange,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: widget.label,
              hintStyle: TextStyle(fontSize: screenHeight(context,dividedBy: 50))
            ),
          ),
        ),
      )
    );
  }
}
