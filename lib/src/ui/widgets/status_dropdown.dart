// import 'package:flutter/material.dart';
// import 'package:hive/hive.dart';
// import 'package:jewel_app/src/models/status_list.dart';
// import 'package:jewel_app/src/utils/urls.dart';
// import 'package:jewel_app/src/utils/utils.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert' as convert;
//
// class StatusDropDown extends StatefulWidget {
//   @override
//   _StatusDropDownState createState() => _StatusDropDownState();
// }
//
// class _StatusDropDownState extends State<StatusDropDown> {
//   String _value;
//   bool isLoading = false;
//   String statusCode;
//   String message;
//   List<String> statusNames = [];
//   List<int> statusIds = [];
//   StatusListResponseModel responseModel = new StatusListResponseModel();
//   int selectedStatusId;
//
//   @override
//   void initState() {
//     setState(() {
//       isLoading = true;
//     });
//     getStatusList();
//     // print("ress"+responseModel.data.toString());
//     super.initState();
//   }
//
//   void getStatusId(){
//    selectedStatusId= statusIds[statusNames.indexOf(_value)];
//    Hive.box("app").put(4, selectedStatusId);
//   }
//
//   void getStatusList() async {
//     var client = http.Client();
//     try {
//       var res = await client.get(
//         Urls.statusListUrl,
//       );
//       setState(() {
//         isLoading = false;
//       });
//       var jsonResponse = convert.jsonDecode(res.body);
//       // statusCode = jsonResponse['status'];
//       message = jsonResponse['message'];
//       print("messs"+jsonResponse.toString());
//       // statusNames=jsonResponse['data']["name"];
//       print(jsonResponse["data"]);
//         await setState(() {
//           responseModel =
//               StatusListResponseModel.fromJson(convert.jsonDecode(res.body));
//         });
//
//         for(int i=0 ;i<responseModel.data.length;i++){
//           statusNames.add(responseModel.data[i].name);
//           statusIds.add(responseModel.data[i].id);
//         }
//         // print("sas"+responseModel.data[0].toString());
//     } finally {
//       client.close();
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return isLoading==false? Row(
//       mainAxisAlignment: MainAxisAlignment.start,
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         SizedBox(
//           width: screenWidth(context, dividedBy: 20),
//         ),
//         // DropdownButton(
//         //   dropdownColor: Colors.white,hint: Text("Select Status"),value: _value,
//         //   underline: Container(),
//         //   items: [
//         //     DropdownMenuItem(
//         //       child: Text(
//         //         "First Item",
//         //         style: TextStyle(color: Colors.blue),
//         //       ),
//         //       value: "First ITEM",
//         //     ),
//         //     DropdownMenuItem(
//         //       child: Text(
//         //         "Second Item",
//         //         style: TextStyle(color: Colors.blue),
//         //       ),
//         //       value: "Second Item",
//         //     ),
//         //     DropdownMenuItem(
//         //         child: Text(
//         //           "Third Item",
//         //           style: TextStyle(color: Colors.blue),
//         //         ),
//         //         value: "Third Item"),
//         //     DropdownMenuItem(
//         //         child: Text(
//         //           "Fourth Item",
//         //           style: TextStyle(color: Colors.blue),
//         //         ),
//         //         value: "Fourth Item")
//         //   ],
//         //   onChanged: (value) {
//         //     setState(() {
//         //       _value = value;
//         //     });
//         //   },
//         // ),
//         DropdownButton<String>(value: _value,hint: Text("Update Status",style: TextStyle(color: Colors.blue),),
//           items:statusNames.map((String value) {
//             return new DropdownMenuItem<String>(
//               value: value,
//               child: new Text(
//                 value,
//                 style: TextStyle(color: Colors.blue),
//               ),
//             );
//           }).toList(),
//           onChanged: (val) {
//           setState(() {
//             _value=val;
//           });
//           getStatusId();
//           },
//         ),
//       ],
//     ):Container();
//   }
// }
