import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/models/customer_response_model.dart';
import 'package:jewel_app/src/utils/utils.dart';

class Remark extends StatefulWidget {
  CustomerResponseModel responseModel;
  Remark({this.responseModel});
  @override
  _RemarkState createState() => _RemarkState();
}

class _RemarkState extends State<Remark> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: screenWidth(context, dividedBy: 8),
            child: Text(
              'Remarks',
              style: TextStyle(
                  fontSize: screenWidth(context, dividedBy: 90),
                  color: Colors.black),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Container(
            height: screenHeight(context, dividedBy: 4),
            width: screenWidth(context, dividedBy: 4),
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                  side: BorderSide(color: Colors.black, width: 1)),
              child: Scrollbar(
                child: ListView.builder(
                  itemCount: widget.responseModel.data.remarks.length,
                  padding: EdgeInsets.only(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return widget.responseModel.data.remarks[index].remarks !=
                            null
                        ? Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(widget
                                    .responseModel.data.remarks[index].date
                                    .toString()
                                    .substring(0, 10),style: TextStyle(fontSize: screenHeight(context,dividedBy: 50)),),
                                Text(widget
                                    .responseModel.data.remarks[index].remarks,style: TextStyle(fontSize: screenHeight(context,dividedBy: 35))),
                              ],
                            ),
                          )
                        : Container();
                  },
                ),
              ),
            ),
          ),
        ),
      ],
    ));
  }
}
