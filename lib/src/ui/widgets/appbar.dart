import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';

class BuildAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(screenWidth(context, dividedBy: 1), 1000),
      child: Card(
        child: Container(
          color: Colors.blue,
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Builder(
              builder: (context) => Row(
                children: [
                  InkWell(
                    child: Icon(
                      Icons.dehaze,
                      color: Colors.white,
                    ),
                    onHover: (value) {},
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {},
                          child: Text(
                            '',
                            style: TextStyle(color: Colors.black),
                          ),
                          onHover: (_) {},
                        ),
                        SizedBox(width: screenWidth(context,dividedBy: 20)),
                        InkWell(
                          onTap: () {},
                          child: Text(
                            '',
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {},
                    child: Text(
                      'USER ID',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context,dividedBy: 50),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Text(
                          'Logout',
                          style: TextStyle(color: Colors.white),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.logout,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
