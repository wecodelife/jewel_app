import 'package:flutter/material.dart';

class BuildSelectIcon extends StatefulWidget {
  final bool selected;
  BuildSelectIcon({this.selected});
  @override
  _BuildSelectIconState createState() => _BuildSelectIconState();
}

class _BuildSelectIconState extends State<BuildSelectIcon> {


  @override
  Widget build(BuildContext context) {
    return widget.selected == false
        ? Icon(
          Icons.radio_button_unchecked,
          color: Colors.blue.withOpacity(.5),
          size: 20,
        )
        : Icon(
          Icons.check_circle,
          color: Colors.blue,
          size: 20,
        );
  }
}
