import 'dart:io';

// import 'package:excel/excel.dart';
// import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/ui/widgets/file_upload.dart';
import 'package:jewel_app/src/utils/utils.dart';

class BuildDrawer extends StatefulWidget {
  @override
  _BuildDrawerState createState() => _BuildDrawerState();
}

class _BuildDrawerState extends State<BuildDrawer> {
  List<String> officerIds = [];

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          GestureDetector(
            child: ListTile(
              title: Text(
                "Import Customers",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              leading: Icon(
                Icons.upload_file,
                color: Colors.blue,
              ),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => FileUploadApp()),
              );
            },
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          // GestureDetector(
          //   child: ListTile(
          //     title: Text(
          //       "Import Officers",
          //       style: TextStyle(fontWeight: FontWeight.bold),
          //     ),
          //     leading: Icon(
          //       Icons.import_export,
          //       color: Colors.orange,
          //       size: 30,
          //     ),
          //     trailing: Icon(
          //       Icons.arrow_forward,
          //       color: Colors.orange,
          //       size: 30,
          //     ),
          //   ),
          //   onTap: () async {
          //     // FilePickerResult result = await FilePicker.platform.pickFiles(
          //     //     type: FileType.custom,
          //     //     allowedExtensions: ['xlsx', 'csv', 'xls']);
          //     //
          //     // if (result != null) {
          //     //   File file = File(result.files.single.path);
          //     //   var filepath = file.path;
          //     //   var bytes = File(filepath).readAsBytesSync();
          //     //   var excel = Excel.decodeBytes(bytes);
          //     //   int i = 0;
          //     //   for (var table in excel.tables.keys) {
          //     //     print(table); //sheet Name
          //     //     print(excel.tables[table].maxCols);
          //     //     print(excel.tables[table].maxRows);
          //     //     for (var row in excel.tables[table].rows) {
          //     //       print(row[0]);
          //     //       Box<Officer> officerBox =
          //     //           Hive.box<Officer>(HiveBoxes.officers);
          //     //       if (officerIds.contains(row[0]) == false) {
          //     //         officerBox.add(
          //     //             Officer(name: row[1], id: row[0], isActive: true));
          //     //         i++;
          //     //       }
          //     //     }
          //     //     showToast(i.toString() + " " + "Officers Imported");
          //     //   }
          //     // }
          //   },
          // ),
        ],
      ),
    );
  }
}
