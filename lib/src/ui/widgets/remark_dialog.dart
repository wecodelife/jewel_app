import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/ui/widgets/raised_button.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class RemarkDialog extends StatefulWidget {
  @override
  _RemarkDialogState createState() => _RemarkDialogState();
}

class _RemarkDialogState extends State<RemarkDialog> {
  String remark;
  bool isLoading = false;
  String statusCode;
  String message;
  TextEditingController remarkController = TextEditingController();
  void updateRemark () async{
    var client = http.Client();
    setState(() {
      isLoading = true;
    });
    try{
      print(Hive.box('app').get(3).toString());
      print(remark);
      var res = await client.post(
          Urls.addRemark,
          headers: {
            "Authorization": "Token " + Hive.box('app').get(1)
          },
          body: {
            'agent': Hive.box('app').get(3).toString(),
            'remarks':remark,
          }
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      statusCode = jsonResponse['status'].toString();
      message = jsonResponse['message'];
      if (statusCode == 200.toString() ||statusCode == 201.toString()) {
        showToast(message);
      }else {
        showToast("Please Check your connection!!");
      }
      print(jsonResponse["data"]);
    }finally{
      client.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Remarks'),
          SizedBox(
            height: screenHeight(context,dividedBy:30),
          ),
          Center(
            child:Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                  side: BorderSide(color: Colors.red, width: 0)),
              child: Container(
                height: screenHeight(context,dividedBy: 2.5),
                width:screenWidth(context,dividedBy: 3),
                child:Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: TextField(
                    autofocus: true,
                    controller: remarkController,
                    onChanged: (value){
                      value = remarkController.text.toString();
                      setState(() {
                        remark = value;
                      });
                    },
                    decoration: InputDecoration(
                      hintText: 'Remarks',
                      hintStyle: TextStyle(fontSize: screenHeight(context,dividedBy: 50)),
                      border: InputBorder.none
                    ),
                  ),
                ),

              ),
            ),
          ),
            SizedBox(
              height: screenHeight(context,dividedBy: 15),
            ),
            Align(
              alignment: Alignment.bottomRight,
            child: BuildRaisedButton(
              backgroundColor: Constants.kitGradients[2],
              buttonName: "Done",
              width: screenWidth(context, dividedBy: 12),
              height: screenWidth(context, dividedBy: 50),
              onPressed: (){
                print(remark);
                updateRemark();
                Navigator.pop(context);
              },
            ),
          )
        ],
      ),
    );
  }
}
