import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/models/customer_response_model.dart';

import 'package:jewel_app/src/models/status_list.dart';
import 'package:jewel_app/src/utils/utils.dart';

class PhoneDrop extends StatefulWidget {
  List<int> phoneNumbers = [];
  final ValueChanged<int> getStatusId;
  final ValueChanged<int> getPhoneId;
  List<int> statusId;
  List<int> phoneId;
  int phoneDropId;
  PhoneDrop({this.phoneNumbers, this.getStatusId, this.statusId, this.phoneId, this.getPhoneId, this.phoneDropId});
  @override
  _PhoneDropState createState() => _PhoneDropState();
}

class _PhoneDropState extends State<PhoneDrop> {
  int _value;
  int i;
  ValueChanged<int> getPhoneId;
  List<int> phoneId;
  // bool isLoading = false;
  // String statusCode;
  // String message;
  List<String> statusNames = ['No PhoneNumber'];
  // List<int> statusIds = [];
  // StatusListResponseModel responseModel = new StatusListResponseModel();
  // int selectedStatusId;

  @override
  void initState() {
    widget.getStatusId(
        widget.statusId[0]);
    widget.getPhoneId(
        widget.phoneId[0]);
    Hive.box('app').put('phone_number', widget.phoneNumbers[0]);
    setState(() {

    });
  }
    // getStatusList();
  //   // print("ress"+responseModel.data.toString());
  //   super.initState();
  // }
  //
  // void getStatusId(){
  //   selectedStatusId= statusIds[statusNames.indexOf(_value)];
  //   Hive.box("app").put(4, selectedStatusId);
  // }
  //
  // void getStatusList() async {
  //   var client = http.Client();
  //   try {
  //     var res = await client.get(
  //       Urls.statusListUrl,
  //     );
  //     setState(() {
  //       isLoading = false;
  //     });
  //     var jsonResponse = convert.jsonDecode(res.body);
  //     // statusCode = jsonResponse['status'];
  //     message = jsonResponse['message'];
  //     print("messs"+jsonResponse.toString());
  //     // statusNames=jsonResponse['data']["name"];
  //     print(jsonResponse["data"]);
  //     await setState(() {
  //       responseModel =
  //           StatusListResponseModel.fromJson(convert.jsonDecode(res.body));
  //     });
  //
  //     for(int i=0 ;i<responseModel.data.length;i++){
  //       statusNames.add(responseModel.data[i].name);
  //       statusIds.add(responseModel.data[i].id);
  //     }
  //     // print("sas"+responseModel.data[0].toString());
  //   } finally {
  //     client.close();
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    // return isLoading==false?
    return Card(
      elevation: 1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 60),
          ),
          // DropdownButton(
          //   dropdownColor: Colors.white,hint: Text("Select Status"),value: _value,
          //   underline: Container(),
          //   items: [
          //     DropdownMenuItem(
          //       child: Text(
          //         "First Item",
          //         style: TextStyle(color: Colors.blue),
          //       ),
          //       value: "First ITEM",
          //     ),
          //     DropdownMenuItem(
          //       child: Text(
          //         "Second Item",
          //         style: TextStyle(color: Colors.blue),
          //       ),
          //       value: "Second Item",
          //     ),
          //     DropdownMenuItem(
          //         child: Text(
          //           "Third Item",
          //           style: TextStyle(color: Colors.blue),
          //         ),
          //         value: "Third Item"),
          //     DropdownMenuItem(
          //         child: Text(
          //           "Fourth Item",
          //           style: TextStyle(color: Colors.blue),
          //         ),
          //         value: "Fourth Item")
          //   ],
          //   onChanged: (value) {
          //     setState(() {
          //       _value = value;
          //     });
          //   },
          // ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<int>(
                value: _value,
                hint: Text(
                   widget.phoneNumbers[0].toString(),
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: screenWidth(context, dividedBy: 50)),
                ),
                items: widget.phoneNumbers.map((int value) {
                  return new DropdownMenuItem<int>(
                    value: value,
                    child: new Text(
                      value.toString(),
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: screenWidth(context, dividedBy: 40)),
                    ),
                  );
                }).toList(),
                onChanged: (int val) {
                  setState(() {
                    _value = val;
                  });
                  Hive.box('app').put('phone_number',_value);
                  print(_value);
                  print(widget.phoneNumbers);
                  widget.getStatusId(
                      widget.statusId[widget.phoneNumbers.indexOf(_value)]);
                  widget.getPhoneId(widget.phoneId[widget.phoneNumbers.indexOf(_value)]);

                },
              ),
            ),
          ),
        ],
      ),
    );
    // ):Container();
  }
}
