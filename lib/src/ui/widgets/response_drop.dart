import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import 'package:jewel_app/src/models/status_list.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;


class ResponseDrop extends StatefulWidget {
  int resonseDropId;
  List<int> responseId;
  final ValueChanged<int> getResponseDropId;
  ResponseDrop({
   this.responseId,
   this.resonseDropId,
   this.getResponseDropId
});
  @override
  _ResponseDropState createState() => _ResponseDropState();
}

class _ResponseDropState extends State<ResponseDrop> {
  String _value;
  bool isLoading = false;
  String statusCode;
  String message;
  List<String> responseNames = [
  ];
  List<int> statusIds = [];
  StatusListResponseModel responseModel = new StatusListResponseModel();
  int selectedStatusId;

  @override
  void initState() {
    setState(() {
      isLoading = true;
    });
    getStatusList();
    // print("ress"+responseModel.data.toString());
    super.initState();
  }

  void getStatusId(){
    selectedStatusId= statusIds[responseNames.indexOf(_value)];
    Hive.box("app").put(4, selectedStatusId);
  }

  void getStatusList() async {
    var client = http.Client();
    try {
      var res = await client.get(
        Urls.responseListUrl,
          headers: {"Authorization": "Token " + Hive.box('app').get(1)}
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      // statusCode = jsonResponse['status'];
      message = jsonResponse['message'];
      print("messs"+jsonResponse.toString());
      // statusNames=jsonResponse['data']["name"];
      print(jsonResponse["data"]);
      await setState(() {
        responseModel =
            StatusListResponseModel.fromJson(convert.jsonDecode(res.body));
      });

      for(int i=0 ;i<responseModel.data.length;i++){
        responseNames.add(responseModel.data[i].name);
        statusIds.add(responseModel.data[i].id);
      }
      // print("sas"+responseModel.data[0].toString());
    } finally {
      client.close();
    }
  }
  void updateResponse () async{
    var client = http.Client();
    setState(() {
      isLoading = true;
    });
    try{
      print(Hive.box('app').get(3).toString());
      print(_value);
      var res = await client.post(
          Urls.updateResponse,
          headers: {
            "Authorization": "Token " + Hive.box('app').get(1)
          },
          body: {
            'agent': Hive.box('app').get(3),
            'status':_value
          }
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      statusCode = jsonResponse['status'].toString();
      message = jsonResponse['message'];
      if (statusCode == 200.toString() ||statusCode == 201.toString()) {
        showToast(message);
        }else {
        showToast("Please Check your connection!!");
      }
      print(jsonResponse["data"]);
    }finally{
      client.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    // return isLoading==false? Row(
    return Card(
      elevation: 1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(width: screenWidth(context,dividedBy: 60),),
          // DropdownButton(
          //   dropdownColor: Colors.white,hint: Text("Select Status"),value: _value,
          //   underline: Container(),
          //   items: [
          //     DropdownMenuItem(
          //       child: Text(
          //         "First Item",
          //         style: TextStyle(color: Colors.blue),
          //       ),
          //       value: "First ITEM",
          //     ),
          //     DropdownMenuItem(
          //       child: Text(
          //         "Second Item",
          //         style: TextStyle(color: Colors.blue),
          //       ),
          //       value: "Second Item",
          //     ),
          //     DropdownMenuItem(
          //         child: Text(
          //           "Third Item",
          //           style: TextStyle(color: Colors.blue),
          //         ),
          //         value: "Third Item"),
          //     DropdownMenuItem(
          //         child: Text(
          //           "Fourth Item",
          //           style: TextStyle(color: Colors.blue),
          //         ),
          //         value: "Fourth Item")
          //   ],
          //   onChanged: (value) {
          //     setState(() {
          //       _value = value;
          //     });
          //   },
          // ),
          DropdownButtonHideUnderline(
            child: DropdownButton<String>(value: _value,hint: Text("Response",style: TextStyle(color: Colors.black,fontSize: screenWidth(context,dividedBy: 60)),),

              items:responseNames.map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child: new Text(
                    value,
                    style: TextStyle(color: Colors.black,fontSize: screenWidth(context,dividedBy: 60)),
                  ),
                );
              }).toList(),
              onChanged: (val) {
                setState(() {
                  _value=val;
                });
                widget.getResponseDropId(statusIds[responseNames.indexOf(_value)]);
                // Hive.box('app').put('response', _value);
                // getStatusId();
              },
            ),
          ),
        ],
      ),
    );
    // ):Container();
  }
}
