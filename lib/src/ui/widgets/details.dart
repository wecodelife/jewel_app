import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/ui/widgets/customer_dialog.dart';
import 'package:jewel_app/src/ui/widgets/raised_button.dart';
import 'package:jewel_app/src/ui/widgets/remark_dialog.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';

class Details extends StatefulWidget {
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {

        return Center(
          child: Container(
            height: screenHeight(context,dividedBy: 1.3),
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                  side: BorderSide(color: Colors.red, width: 0)),
              content: RemarkDialog(),
              actions: <Widget>[],
            ),
          ),
        );
      },
    );
  }
  void _showDialog1() {
    showDialog(
      context: context,
      builder: (BuildContext context) {

        return Center(
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
                side: BorderSide(color: Colors.red, width: 0)),
            content: CustomerDialog(),
            actions: <Widget>[],
          ),
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child:Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: screenWidth(context,dividedBy: 8),
              child: Text('Add Details',
                style: TextStyle(fontSize: screenWidth(context,dividedBy: 90),color: Colors.black),),
            ),
          ),
          BuildRaisedButton(
            backgroundColor: Constants.kitGradients[2],
            buttonName: "Add Remarks",
            width: screenWidth(context, dividedBy: 10),
            height: screenWidth(context, dividedBy: 50),
            onPressed:() {
              _showDialog();
            },
          ),
          SizedBox(
            width: screenWidth(context,dividedBy: 60),
          ),
          BuildRaisedButton(
            backgroundColor: Constants.kitGradients[2],
            buttonName: "Add Customer",
            width: screenWidth(context, dividedBy: 10),
            height: screenWidth(context, dividedBy: 50),
            onPressed: (){
              _showDialog1();
            },
          )
        ],
      )
    );
  }
}
