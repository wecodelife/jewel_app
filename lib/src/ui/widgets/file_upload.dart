import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;
import 'dart:typed_data';
import 'dart:async';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' as http;
import 'package:jewel_app/src/ui/screens/login.dart';
import 'package:jewel_app/src/ui/screens/success_page.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
// import 'package:web_upload/apps/main_app.dart';

class FileUploadApp extends StatefulWidget {
  @override
  createState() => _FileUploadAppState();
}

class _FileUploadAppState extends State<FileUploadApp> {
  List<int> _selectedFile;
  var box = Hive.box('app');
  Uint8List _bytesData;
  bool loading = false;
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  startWebFilePicker() async {
    html.InputElement uploadInput = html.FileUploadInputElement();
    uploadInput.multiple = true;
    uploadInput.draggable = true;
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      final files = uploadInput.files;
      final file = files[0];
      final reader = new html.FileReader();

      reader.onLoadEnd.listen((e) {
        _handleResult(reader.result);
      });
      reader.readAsDataUrl(file);
    });
  }

  void _handleResult(Object result) {
    setState(() {
      _bytesData = Base64Decoder().convert(result.toString().split(",").last);
      _selectedFile = _bytesData;
    });
  }

  Future<String> makeRequest() async {
    setState(() {
      loading = true;
    });
    var url = Uri.parse(Urls.uploadFileUrl);
    var request = new http.MultipartRequest("POST", url);
    request.files.add(await http.MultipartFile.fromBytes('file', _selectedFile,
        contentType: new MediaType('application', 'octet-stream'),
        filename: "file_up"));

    request.send().then((response) {
      setState(() {
        loading = false;
      });
      print("test");
      print(response.statusCode);

      if (response.statusCode == 200 || response.statusCode==201) {
        print("Uploaded!");
        showDialog(
            barrierDismissible: false,
            context: context,
            child: new AlertDialog(
              title: new Text("Details"),
              //content: new Text("Hello World"),
              content: new SingleChildScrollView(
                child: new ListBody(
                  children: <Widget>[
                    new Text("Upload successfull"),
                  ],
                ),
              ),
              actions: <Widget>[
                new FlatButton(
                  child: new Text('OK'),
                  onPressed: () {
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => SuccessPage()),
                      (Route<dynamic> route) => false,
                    );
                    // Navigator.pop(context);
                  },
                ),
              ],
            ));
      } else {
        showToast("Upload failed");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var screenSize = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(screenSize.width, 1000),
      child: Card(
        child: Container(
          color: Colors.blue,
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Builder(
              builder: (context) => Row(
                children: [
                  InkWell(
                    child: Icon(
                      Icons.dehaze,
                      color: Colors.white,
                    ),
                    onHover: (value) {},
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {},
                          child: Text(
                            '',
                            style: TextStyle(color: Colors.black),
                          ),
                          onHover: (_) {},
                        ),
                        SizedBox(width: screenSize.width / 20),
                        InkWell(
                          onTap: () {},
                          child: Text(
                            '',
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {},
                    child: Text(
                      box.get(2).toString().toUpperCase(),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    width: screenSize.width / 50,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
                        ModalRoute.withName('/'),
                      );
                    },
                    child: Row(
                      children: [
                        Text(
                          'Logout',
                          style: TextStyle(color: Colors.white),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.logout,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    ),
        body: loading == false
            ? Container(
                child: new Form(
                  autovalidate: true,
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 16.0, left: 28),
                    child: new Container(
                        width: 350,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    MaterialButton(
                                      color: Colors.blue,
                                      elevation: 8,
                                      highlightElevation: 2,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      textColor: Colors.white,
                                      child: Text('Select a file'),
                                      onPressed: () {
                                        startWebFilePicker();
                                      },
                                    ),
                                    Divider(
                                      color: Colors.teal,
                                    ),
                                    RaisedButton(
                                      color: Colors.blue,
                                      elevation: 8.0,
                                      textColor: Colors.white,
                                      onPressed: () {
                                        makeRequest();
                                      },
                                      child: Text('Send file to server'),
                                    ),
                                  ])
                            ])),
                  ),
                ),
              )
            : Container(
                child: Center(child: CircularProgressIndicator()),
              ),
      ),
    );
  }
}
