import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';

class BuildRaisedButton extends StatefulWidget {
  final String buttonName;
  final Color backgroundColor;
  final double height;
  final double width;
  final Function onPressed;
  final bool isLoading;

  BuildRaisedButton(
      {this.buttonName,
      this.backgroundColor,
      this.height,
      this.width,
      this.onPressed,
      this.isLoading});
  @override
  _BuildRaisedButtonState createState() => _BuildRaisedButtonState();
}

class _BuildRaisedButtonState extends State<BuildRaisedButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(9)),
        onPressed: () {
          widget.onPressed();
        },
        child: widget.isLoading == true
            ? Container(
                child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),),
                height: widget.height / 2,
                width: widget.height / 2,
              )
            : Text(
                widget.buttonName,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: screenWidth(context, dividedBy: 100)),
              ),
        color: widget.backgroundColor,
      ),
    );
  }
}
