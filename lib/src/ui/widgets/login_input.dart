import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';

class BuildLoginInput extends StatefulWidget {
  final String fieldName;
  final String iconName;
  final TextEditingController textEditingController;
  final bool obs;
  final bool numKey;
  final Function onChange;

  BuildLoginInput(
      {this.fieldName,
      this.iconName,
      this.textEditingController,
      this.obs,
      this.onChange,
      this.numKey});

  @override
  _BuildLoginInputState createState() => _BuildLoginInputState();
}

class _BuildLoginInputState extends State<BuildLoginInput> {
  bool showText = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: screenHeight(context, dividedBy: 12),
      width: screenWidth(context, dividedBy: 2.3),

      child: TextField(cursorColor: Colors.black,
        obscureText: widget.obs != null ? showText : false,

        keyboardType:
            widget.numKey == true ? TextInputType.phone : TextInputType.text,
        controller: widget.textEditingController,
        onChanged: (_) {
          widget.onChange();
        },
        style: TextStyle(
            fontSize: screenWidth(context, dividedBy: 80), color: Colors.black),
        decoration: InputDecoration(
            labelText: widget.fieldName,
            labelStyle: TextStyle(color: Colors.black,fontSize: screenWidth(context,dividedBy: 100)),
            suffixIcon: widget.obs != null
                ? GestureDetector(
                    child: showText == false
                        ? Icon(
                            Icons.visibility,
                            color: Colors.black.withOpacity(.50),
                            size: screenWidth(context, dividedBy: 100),
                          )
                        : Icon(
                            Icons.visibility_off,
                            color: Colors.black.withOpacity(.50),
                            size: screenWidth(context, dividedBy: 100),
                          ),
                    onTap: () {
                      if (showText) {
                        setState(() {
                          showText = false;
                        });
                      } else {
                        setState(() {
                          showText = true;
                        });
                      }
                    })
                : SizedBox(
                    width: 0,
                    height: 0,
                  ),
//            hintText:widget.fieldName ,
//            border: InputBorder.none,
            hintStyle: TextStyle(color: Colors.black, fontSize: 14)),
      ),
    );
  }
}
