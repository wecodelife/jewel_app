import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/ui/widgets/raised_button.dart';
import 'package:jewel_app/src/ui/widgets/select_deselect_icon.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class OthersField extends StatefulWidget {
  final int questionId;
  final int optionId;
  OthersField({
    this.questionId,
    this.optionId,
});
  @override
  _OthersFieldState createState() => _OthersFieldState();
}


class _OthersFieldState extends State<OthersField> {
  bool selected = false;
  bool isSubmitted = false;
  bool isLoading=false;
  bool readOnly = true;
  String other;
  String statusCode;
  String message;
  String customerId;
  String token;
  TextEditingController otherTextEditingController = TextEditingController();
  void _select(){
    Hive.box('app').get('questionId'+widget.questionId.toString()) !=null ?
setState(() {
  selected = !selected;
  readOnly = !readOnly;
})
    : showToast('Please Select An Option to Add Remarks');
  }

  @override
  void initState() {
    Hive.box('app').delete('questionId'+widget.questionId.toString());

    customerId=Hive.box('app').get(3).toString();
    token=Hive.box('app').get(1).toString();
    super.initState();
  }
  void answerQuestionRemark() async {
    await Hive.openBox('app');
    setState(() {
      isLoading = true;
    });

    var client = http.Client();
    print(widget.questionId);
    try {
      var res = await client.post(
        Urls.addQuestionRemark,body: {
        "agent":Hive.box('app').get(3).toString(),
        "question":widget.questionId.toString(),
        "remarks":other
      },
        headers: {"Authorization": "Token " + token},
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      statusCode = jsonResponse['status'].toString();
      message = jsonResponse['message'];
      if(statusCode == 200.toString() || statusCode == 201.toString())
      setState(() {
        isSubmitted = true;
        readOnly=!readOnly;
        selected=!selected;
      });
      showToast(message);

    } finally {
      client.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    return
      Hive.box('app').get('questionId'+widget.questionId.toString()) !=null ?
      isLoading==false? Padding(
      padding: EdgeInsets.symmetric(
          vertical: screenWidth(
            context,
            dividedBy: 200,
          ),
          horizontal: screenWidth(
            context,
            dividedBy: 50,
          )),
    child: GestureDetector(
      child: Row(
        children: [
          BuildSelectIcon(
            selected: selected,
          ),
          selected == false ? Container(
              width: screenWidth(context, dividedBy: 4),
              child: Text(
                isSubmitted == true ? other : 'Remarks',
                textAlign: TextAlign.justify,
                style: TextStyle(
                    fontSize: screenWidth(context, dividedBy: 100),height: 1.5,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.italic),
              )):Container(
            width: screenWidth(context,dividedBy: 7),
            child: Center(
              child: Padding(
                padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 100)),
                child: TextField(
                  maxLines: null,
                  onChanged: (value) {
                     other = value;
                  },
                  controller: otherTextEditingController,
                  readOnly: readOnly,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)
                    ),
                    hintText: 'Remark',
                    hintStyle:TextStyle(fontSize: 10) ,
                  ),
                ),
              ),
            ),

          ),
          SizedBox(width: screenWidth(context,dividedBy: 80),),
          selected == false ? Container() : BuildRaisedButton(
            backgroundColor: Colors.red,
            buttonName: "Submit",
            width: screenWidth(context, dividedBy: 18),
            height: screenWidth(context, dividedBy: 60),
            onPressed: (){
              print(other);
              answerQuestionRemark();

            },
          )
        ],
      ),
        onTap: () {
          _select();
        }),
    ):Container(width:5, height: 50,child: Center(child: CircularProgressIndicator())):Container();
  }
}
