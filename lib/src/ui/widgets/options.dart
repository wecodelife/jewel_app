import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/models/select_answer_request_model.dart';
import 'package:jewel_app/src/ui/widgets/select_deselect_icon.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Options extends StatefulWidget {
  final int index;
  final int parentIndex;
  final String optionContent;
  final int optionId;
  final int questionId;
  final bool selected;
  final ValueChanged<int> selectFunction;
  final ValueChanged<int> getParentIndex;
  final ValueChanged<int> getOptionVal;

  Options(
      {this.index,
      this.selected,
      this.selectFunction,
      this.getParentIndex,
      this.parentIndex,this.optionContent,this.getOptionVal,this.optionId,this.questionId});
  @override
  _OptionsState createState() => _OptionsState();
}

class _OptionsState extends State<Options> {
  var box = Hive.box('app');
  bool isLoading=false;
  String message;
  String statusCode;
  String customerId;
  String token;
  bool selected=false;
  bool others;
  @override
  void initState() {
    customerId=box.get(3).toString();
    token=box.get(1).toString();
    Hive.box('app').put('questionID'+widget.questionId.toString(),null);
    super.initState();
  }


  void answerQuestion(bool unSelect) async {
    await Hive.openBox('app');
    setState(() {
      isLoading = true;
    });

    var client = http.Client();

    try {
      var res = await client.post(
        Urls.answerQuestionUrl,body: {
          "agent":Hive.box('app').get(3).toString(),
        "question":widget.questionId.toString(),
        "option":unSelect==false?widget.optionId.toString():null,

      },
        headers: {"Authorization": "Token " + token},
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      statusCode = jsonResponse['status'].toString();
      message = jsonResponse['message'];
      showToast(message);

    } finally {
      client.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    return isLoading==false? Padding(
      padding: EdgeInsets.symmetric(
          vertical: screenWidth(
            context,
            dividedBy: 200,
          ),
          horizontal: screenWidth(
            context,
            dividedBy: 50,
          )),
      child: GestureDetector(
          child: Row(
            children: [
              BuildSelectIcon(
                selected: widget.selected,
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 500),
              ),
              Container(
                  width: screenWidth(context, dividedBy: 4),
                  child: Text(
                    widget.optionContent,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: screenWidth(context, dividedBy: 100),height: 1.5,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.italic),
                  )),
            ],
          ),
          onTap: () {
            if(selected==false) {
              Hive.box('app').put('questionId'+widget.questionId.toString(), 'success');
              // Hive.box('app').put(
              //     'questionID' + widget.questionId.toString(),null);
              // print(Hive.box('app').get(
              //     'questionID' + widget.questionId.toString()));
              answerQuestion(false);
            }
            else
              // Hive.box('app').put('questionID'+widget.questionId.toString(),'success');
            // print(Hive.box('app').get(
            //     'questionID' + widget.questionId.toString()));
              print('true');
              answerQuestion(true);
            widget.getParentIndex(widget.parentIndex);
            widget.selectFunction(widget.index);
            widget.getOptionVal(widget.optionId);
          }),
    ):Container(width:5, height: 50,child: Center(child: CircularProgressIndicator()));
  }
}
