// import 'dart:html';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:jewel_app/src/ui/widgets/appbar.dart';
// import 'package:jewel_app/src/ui/widgets/details.dart';
// import 'package:jewel_app/src/ui/widgets/drop_down.dart';
// import 'package:jewel_app/src/ui/widgets/phone_drop.dart';
// import 'package:jewel_app/src/ui/widgets/question_answer.dart';
// import 'package:jewel_app/src/ui/widgets/questions.dart';
// import 'package:jewel_app/src/ui/widgets/raised_button.dart';
// import 'package:jewel_app/src/ui/widgets/remark.dart';
// import 'package:jewel_app/src/ui/widgets/remark_dialog.dart';
// import 'package:jewel_app/src/ui/widgets/response_drop.dart';
// import 'package:jewel_app/src/ui/widgets/text_field.dart';
// import 'package:jewel_app/src/utils/utils.dart';
//
// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }
//
// class _HomeScreenState extends State<HomeScreen> {
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: Size(screenWidth(context, dividedBy: 1), 70),
//         child: Container(
//           color: Colors.red,
//           child: Padding(
//             padding: EdgeInsets.all(20),
//             child: Builder(
//               builder: (context) => Row(
//                 children: [
//                   InkWell(
//                     child: Icon(
//                       Icons.circle,
//                       color: Colors.white,
//                     ),
//
//                     onTap: () {
//
//                     },
//                   ),
//                   Text('Telecaller',
//                   style: TextStyle(color: Colors.white),),
//                   Expanded(
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         InkWell(
//                           onTap: () {},
//                           child: Text(
//                             '',
//                             style: TextStyle(color: Colors.black),
//                           ),
//                           onHover: (_) {},
//                         ),
//                         SizedBox(width: screenWidth(context,dividedBy: 20)),
//                         InkWell(
//                           onTap: () {},
//                           child: Text(
//                             '',
//                             style: TextStyle(color: Colors.black),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   InkWell(
//                     onTap: () {},
//                     child: Text(
//                       'AKHIL E A',
//                       style: TextStyle(color: Colors.white),
//                     ),
//                   ),
//                   SizedBox(
//                     width: screenWidth(context,dividedBy: 50),
//                   ),
//                   InkWell(
//                     onTap: () {
//                       Navigator.pop(context);
//                     },
//                     child: Container(
//                       decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(50),
//                         color: Colors.white,
//
//                       ),
//                       child: Padding(
//                         padding:  EdgeInsets.all(8.0),
//                         child: Text('Logout'),
//                       ),
//                     )
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//       body:Center(
//         child:Padding(
//           padding:  EdgeInsets.all(10),
//           child: ClipRRect(
//             borderRadius: BorderRadius.circular(5.0),
//             child: Container(
//               height:screenHeight(context,dividedBy: 1.1),
//               width: screenWidth(context,dividedBy: 1.1),
//                 margin: const EdgeInsets.only(bottom: 6.0),
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(50),
//                     bottomLeft: Radius.circular(50),
//                     topRight: Radius.circular(50),
//                     bottomRight: Radius.circular(50)
//                 ),
//               ),
//
//               child: Row(
//                 children: [
//                   Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       SizedBox(height: screenHeight(context,dividedBy: 30),),
//               Align(
//                   alignment: Alignment.topLeft,
//                   child: Container(
//                       child:Row(
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         children: [
//                           Padding(
//                             padding: const EdgeInsets.all(8.0),
//                             child: Container(
//                               width: screenWidth(context,dividedBy: 8),
//                               child: Text('Name',
//                                 style: TextStyle(fontSize: 14,color: Colors.black),),
//                             ),
//
//                           ),
//                           Padding(
//                             padding:  EdgeInsets.all(8.0),
//                             child: Container(
//                               child: Text('AKHIL E A',
//                                 style: TextStyle(fontSize: 18,color: Colors.black,fontWeight: FontWeight.w800),),
//                             ),
//                           ),
//
//                         ],
//                       )
//                   )),
//                       SizedBox(height: screenHeight(context,dividedBy: 40),),
//                       Row(children: [
//                         Padding(
//                           padding: const EdgeInsets.all(8.0),
//                           child: Container(
//                             width: screenWidth(context,dividedBy: 8),
//                             child: Text('Phone Number',
//                               style: TextStyle(fontSize: 14,color: Colors.black),),
//                           ),
//                         ),
//                         PhoneDrop(),
//                         SizedBox(width: screenWidth(context,dividedBy: 20),),
//                         StatusDrop()
//
//                       ],),
//
//                       BuildTextField(
//                         label: 'Group',
//                         value: 'SNDP',
//                       ),
//
//                       BuildTextField(
//                         label: 'Area',
//                         value: 'Kottayam',
//                       ),
//
//                       Remark(),
//                       SizedBox(height: screenHeight(context,dividedBy: 40),),
//                       Details(),
//                       SizedBox(height: screenHeight(context,dividedBy: 40),),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           ResponseDrop(),
//                           BuildRaisedButton(
//                             backgroundColor: Colors.red,
//                             buttonName: "Submit",
//                             width: screenWidth(context, dividedBy: 15),
//                             height: screenWidth(context, dividedBy: 50),
//                           )
//
//                         ],
//                       )
//                     ],
//                   ),
//                   Center(
//                     child: Padding(
//                       padding:  EdgeInsets.all(3),
//                       child: Container(
//                         height: screenHeight(context,dividedBy: 1.2),
//                         width: screenWidth(context,dividedBy: 500),
//                         color: Colors.black,
//                       ),
//                     ),
//                   ),
//                   QuestionAnswer()
//                 ],
//               ),
//             ),
//           ),
//         ),
//       )
//     );
//   }
// }
