import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/bloc/user_bloc.dart';
import 'package:jewel_app/src/models/login_request_model.dart';
import 'package:jewel_app/src/models/login_response_model.dart';
import 'package:jewel_app/src/ui/screens/home_page.dart';
import 'package:jewel_app/src/ui/screens/home_screen.dart';
import 'package:jewel_app/src/ui/widgets/login_input.dart';
import 'package:jewel_app/src/ui/widgets/raised_button.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;

import 'dart:convert' as convert;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController textEditingControllerUserId =
      new TextEditingController();
  TextEditingController textEditingControllerPassword =
      new TextEditingController();
  bool isLoading = false;
  bool loggedIn = false;
  // Response response;
  // Dio dio = new Dio();
  SharedPreferences _sharedPreferences;

  setInstance() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  void login() async {
    if (textEditingControllerUserId.text.trim() != "" &&
        textEditingControllerPassword.text.trim() != "") {
      setState(() {
        isLoading = true;
      });
      String message = "Something went wrong!! check your connection";
      String token = "";
      int statusCode;
      var client = http.Client();
      try {
        var res = await client.post(Urls.loginUrl, body: {
          'username': textEditingControllerUserId.text,
          'password': textEditingControllerPassword.text
        });
        setState(() {
          isLoading = false;
        });
        textEditingControllerUserId.text = "";
        textEditingControllerPassword.text = "";
        var jsonResponse = convert.jsonDecode(res.body);
        statusCode = jsonResponse['status'];
        message = jsonResponse['message'];
        print(jsonResponse['data']);
        if (statusCode == 200) {
          token = jsonResponse['data']["token"];
          print(token);
          var box = Hive.box('app');
          box.put(1, token);
          box.put(2, jsonResponse['data']["user"]["username"]);
        }
        // else showToast("Please check user id and password");

      } finally {
        client.close();
      }
      if (statusCode == 200) {
        // ObjectFactory().prefs.setAuthToken(token: "Token " + token);
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => HomePage(
                    token: token,
                  )),
          ModalRoute.withName('/'),
        );
        // showToast(response.data["message"]);

      } else
        showToast(message);
    } else {
      showToast("Please enter user is and password");
    }
  }

  // @override
  // void initState() {
  //   userBlocSingle.loginResponse.listen((event) {
  //     setState(() {
  //       isLoading = false;
  //     });
  //     if (event.status == 200) {
  //       print("SSSS");
  //       setState(() {
  //         loggedIn = true;
  //       });
  //       if (event.data.token != null || event.data.token.isNotEmpty) {
  //         print(event.data.token);
  //         // ObjectFactory().prefs.setAuthToken(
  //         //     token: event.data.token.toString(),
  //         //     sharedPreferences: _sharedPreferences);
  //       }
  //       Navigator.pushAndRemoveUntil(
  //         context,
  //         MaterialPageRoute(builder: (BuildContext context) => HomePage()),
  //         ModalRoute.withName('/'),
  //       );
  //       // showToast(response.data["message"]);
  //
  //     } else
  //       showToast("Please check user id and password");
  //   }, onError: (e) {
  //     setState(() {
  //       isLoading = false;
  //     });
  //     showToast("Please check user id and password");
  //   });
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(

          children: [
            SizedBox(height: screenHeight(context,dividedBy: 20),),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                  ),
              child: Container(
                width: screenWidth(context,dividedBy: 1.055),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: screenHeight(context, dividedBy: 1.2),
                      width: screenWidth(context, dividedBy: 2.3),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Image.network('https://i.pinimg.com/originals/47/ba/16/47ba16d975a5de6afb7ce150184e705c.jpg',
                        height: screenHeight(context,dividedBy: 5),
                        width: screenWidth(context,dividedBy: 2.4),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            bottomLeft: Radius.circular(50),
                            // topRight: Radius.circular(50),
                            // bottomRight: Radius.circular(50)
                        ),

                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 50),
                          vertical: screenWidth(context, dividedBy: 200)),
                      child: Card(
                        child: Container(
                          height: screenHeight(context, dividedBy: 1.3),
                          width: 2,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Padding(
                      padding:  EdgeInsets.all(screenHeight(context,dividedBy: 100)),
                      child: Container(
                        width: screenWidth(context, dividedBy: 2.3),
                        height: screenHeight(context, dividedBy: 1.2),
                        child: Padding(
                          padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:80 )),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: screenHeight(context,dividedBy: 7),
                              ),
                              Text(
                                "Login",
                                style: TextStyle(
                                    color: Colors.black,
                                    backgroundColor: Colors.white,
                                    fontSize: screenWidth(context, dividedBy: 40),
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 100),
                              ),
                              Text('Please login to continue',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: screenWidth(context,dividedBy: 100)
                                ),),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 20),
                              ),
                              BuildLoginInput(
                                fieldName: "Username",
                                textEditingController: textEditingControllerUserId,
                                onChange: (){},

                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 20),
                              ),
                              BuildLoginInput(
                                fieldName: "Password",
                                textEditingController:
                                textEditingControllerPassword,
                                onChange: (){},
                                obs: true,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 15),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  BuildRaisedButton(
                                    backgroundColor: Constants.kitGradients[2],
                                    buttonName: "Login",
                                    isLoading: isLoading,
                                    width: screenWidth(context, dividedBy: 6),
                                    height: screenWidth(context, dividedBy: 30),
                                    onPressed: () async {
                                      // Navigator.pushAndRemoveUntil(
                                      //   context,
                                      //   MaterialPageRoute(builder: (BuildContext context) => HomeScreen()),
                                      //   ModalRoute.withName('/'),
                                      // );
                                      login();
                                      // if (loggedIn == true) {
                                      //   // Navigator.push(
                                      //   //   context,
                                      //   //   MaterialPageRoute(
                                      //   //       builder: (context) => HomePage()),
                                      //   // );
                                      //   Navigator.pushAndRemoveUntil(
                                      //     context,
                                      //     MaterialPageRoute(builder: (BuildContext context) => HomePage()),
                                      //     ModalRoute.withName('/'),
                                      //   );
                                      // }
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),






                  ],
                ),
              ),
            ),
            Text('Powered By'),
             // Image.network('web/icons/wcl.png',height: screenHeight(context,dividedBy: 25),
             // width: screenWidth(context,dividedBy: 10),)
            Text('WeCodeLife',style: TextStyle(fontSize: screenHeight(context,dividedBy: 30)),)
          ],
        ),
      ),
    );
  }
}
