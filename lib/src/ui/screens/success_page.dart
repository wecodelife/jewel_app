import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/ui/screens/home_page.dart';
import 'package:jewel_app/src/ui/screens/login.dart';
import 'package:jewel_app/src/ui/widgets/raised_button.dart';
import 'package:jewel_app/src/utils/utils.dart';

class SuccessPage extends StatefulWidget {
  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  var box = Hive.box('app');

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 1000),
        child: Card(
          child: Container(
            color: Colors.blue,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Builder(
                builder: (context) => Row(
                  children: [
                    InkWell(
                      child: Icon(
                        Icons.dehaze,
                        color: Colors.white,
                      ),
                      onHover: (value) {},
                      onTap: () {
                        Scaffold.of(context).openDrawer();
                      },
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Text(
                              '',
                              style: TextStyle(color: Colors.black),
                            ),
                            onHover: (_) {},
                          ),
                          SizedBox(width: screenSize.width / 20),
                          InkWell(
                            onTap: () {},
                            child: Text(
                              '',
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      child: Text(
                        box.get(2).toString().toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      width: screenSize.width / 50,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => LoginPage()),
                          ModalRoute.withName('/'),
                        );
                      },
                      child: Row(
                        children: [
                          Text(
                            'Logout',
                            style: TextStyle(color: Colors.white),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.logout,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      body: Container(
        width: screenSize.width,
        height: screenSize.height,
        child: Column(
          children: [
            Container(
              width: screenSize.width / 8,
              height: screenSize.width / 8,
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.white70),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Success",
                    style: TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.bold),
                  ),
                  Icon(
                    Icons.check_circle,
                    color: Colors.blue,
                  )
                ],
              ),
            ),
            BuildRaisedButton( width: screenWidth(context, dividedBy: 6),
              height: screenWidth(context, dividedBy: 40),backgroundColor: Colors.blue,buttonName: "Go Home",onPressed: (){
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => HomePage()),
                  ModalRoute.withName('/'),
                );
              },)
          ],
        ),
      ),
    );
  }
}
