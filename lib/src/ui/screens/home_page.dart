import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/models/customer_details_response_model.dart';
import 'package:jewel_app/src/models/customer_response_model.dart';
import 'package:jewel_app/src/models/update_phone_status_request_model.dart';
import 'package:jewel_app/src/ui/screens/login.dart';
import 'package:jewel_app/src/ui/widgets/detail_card.dart';
import 'package:jewel_app/src/ui/widgets/details.dart';
import 'package:jewel_app/src/ui/widgets/drawer.dart';
import 'package:jewel_app/src/ui/widgets/drop_down.dart';
import 'package:jewel_app/src/ui/widgets/phone_drop.dart';
import 'package:jewel_app/src/ui/widgets/question_answer.dart';
import 'package:jewel_app/src/ui/widgets/raised_button.dart';
import 'package:jewel_app/src/ui/widgets/remark.dart';
import 'package:jewel_app/src/ui/widgets/response_drop.dart';
import 'package:jewel_app/src/ui/widgets/text_field.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class HomePage extends StatefulWidget {
  final String token;
  HomePage({this.token});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isLoading = false;
  int statusCode;
  String message;
  String statusMsg;
  CustomerResponseModel responseModel ;
  var box = Hive.box('app');
  List<int> phoneNumberList =[];
  List<int> statusIdList = [];
  int statusId;
  int responseId;
  int responseDropId;
  int statusDropId;
  int phoneNumberId;
  List<int> phoneNumberIdList = [];
  int _statusId;
  int _phoneId;
  int _responseId;




  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading = true;

    });

    getCustomer();

_statusId = statusDropId;
  }


  void getCustomer() async {
    await Hive.openBox('app');
    var client = http.Client();

    try {
      var res = await client.get(
        Urls.getCustomerUrl,
        headers: {"Authorization": "Token " + box.get(1)},
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      statusCode = jsonResponse['status'];
      message = jsonResponse['message'];
      print(jsonResponse["data"]);
      if(jsonResponse['message']!="No data found")
      setState(() {
        responseModel =
            CustomerResponseModel.fromJson(convert.jsonDecode(res.body));
      });
      box.put(3, responseModel.data.id);
      print(responseModel.data);
      for(var i=0;i<=responseModel.data.number.length; i++){
        phoneNumberList.add(responseModel.data.number[i].phoneNumber);
        statusIdList.add(responseModel.data.number[i].status);
        phoneNumberIdList.add(responseModel.data.number[i].id);
      }
    } finally {
      client.close();
    }

  }
  void updateResponse () async{
    var client = http.Client();
    setState(() {
      isLoading = true;
    });
    try{
      print(Hive.box('app').get(3));
      print(_responseId);
      var res = await client.post(
          Urls.updateResponse,
          headers: {
            "Authorization": "Token " + Hive.box('app').get(1)
          },
          body: {
            'agent': Hive.box('app').get(3).toString(),
            'status':_responseId.toString()
          }
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      statusMsg = jsonResponse['status'].toString();
      message = jsonResponse['message'];
      if (statusMsg == 200.toString() ||statusMsg == 201.toString()) {
        showToast(message);
      }else {
        showToast("Please Check your connection!!");
      }
      print(jsonResponse["data"]);
    }finally{
      client.close();
    }
  }
  void updatePhoneStatus () async{
    var client = http.Client();
    setState(() {
      isLoading = true;
    });
    try{
      print(Hive.box('app').get('phone_number'));
      print(Hive.box('app').get(4));
      var res = await client.post(
          Urls.updateStatus,
          headers: {
            "Authorization": "Token " + Hive.box('app').get(1)
          },
          body:{
            "status":statusDropId.toString(),
            "phone_number":Hive.box('app').get('phone_number').toString(),
          }

      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      statusMsg = jsonResponse['status'].toString();
      message = jsonResponse['message'];
      if (statusMsg == 200.toString() ||statusMsg == 201.toString()) {
        showToast(message);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomePage(token: Hive.box('app').get(1),)),
                (Route<dynamic> route) => false,
        );
      }else {
        showToast("Please Check your connection!!");
      }
      print(jsonResponse["data"]);
    }finally{
      client.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: BuildDrawer(),
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100),
        child: Card(
          child: Container(
            color: Constants.kitGradients[2],
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Builder(
                builder: (context) => Row(
                  children: [
                    InkWell(
                      child: Icon(
                        Icons.person_pin,
                        color: Colors.white,
                      ),
                      onHover: (value) {},
                      onTap: () {
                        Scaffold.of(context).openDrawer();
                      },
                    ),
                    Text('Telecaller',
                    style: TextStyle(color: Colors.white,fontSize: screenWidth(context,dividedBy: 60)),),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Text(
                              '',
                              style: TextStyle(color: Colors.black),
                            ),
                            onHover: (_) {},
                          ),
                          SizedBox(width: screenSize.width / 20),
                          InkWell(
                            onTap: () {},
                            child: Text(
                              '',
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      child: Text(
                        box.get(2).toString().toUpperCase()!=null?box.get(2).toString().toUpperCase():"",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      width: screenSize.width / 50,
                    ),
                    InkWell(
                      onTap: () {
                        Hive.box('app').delete(1);
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
                          ModalRoute.withName('/'),
                        );
                      },
                      child: Row(
                        children: [
                          Container(
                            height: screenHeight(context,dividedBy: 25),
                            width: screenWidth(context,dividedBy: 20),
                            decoration:BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.white,
                            ),
                            child: Center(
                              child: Text(
                                'Logout',
                                style: TextStyle(color: Colors.black,fontSize: screenWidth(context,dividedBy: 100)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      body: isLoading == false
          ? Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 95)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: screenHeight(context,dividedBy: 30),),
                      Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                              child:Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      width: screenWidth(context,dividedBy: 8),
                                      child: Text('Name',
                                        style: TextStyle(fontSize: screenWidth(context,dividedBy: 90),color: Colors.black),),
                                    ),

                                  ),
                                  Padding(
                                    padding:  EdgeInsets.all(8.0),
                                    child: Container(
                                      child: Text(responseModel.data.name,
                                        style: TextStyle(fontSize: screenWidth(context,dividedBy: 50),color: Colors.black,fontWeight: FontWeight.w800),),
                                    ),
                                  ),

                                ],
                              )
                          )),
                      SizedBox(height: screenHeight(context,dividedBy: 40),),
                      Row(children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: screenWidth(context,dividedBy: 8),
                            child: Text('Phone Number',
                              style: TextStyle(fontSize: screenWidth(context,dividedBy: 90),color: Colors.black),),
                          ),
                        ),
                        PhoneDrop(
                          phoneNumbers: phoneNumberList,
                          statusId: statusIdList,
                          phoneId: phoneNumberIdList,
                          getStatusId: (value){
                            setState(() {
                              statusId = value;
                            });

                            print(statusId);
                          },
                          getPhoneId: (value){
                            setState(() {
                              phoneNumberId = value;
                            });
                            _phoneId = value;
                          },
                          phoneDropId: phoneNumberId,
                        ),
                        SizedBox(width: screenWidth(context,dividedBy: 20),),
                        StatusDrop(statusDropId: statusId,statusId: statusIdList,getStatusDropId: (value)async{
                          print(value.toString() +"bbbb");
                          setState(() {
                            statusDropId = int.parse(value.toString());
                          });
                          Hive.box('app').put('status', value);
                          _statusId = int.parse(value.toString());
                        },)

                      ],),

                      BuildTextField(
                        label: 'Group',
                        value: responseModel.data.groupName,
                      ),

                      BuildTextField(
                        label: 'Area',
                        value: responseModel.data.areaName,
                      ),

                      Remark(
                        responseModel: responseModel,
                      ),
                      SizedBox(height: screenHeight(context,dividedBy: 40),),
                      Details(),
                      SizedBox(height: screenHeight(context,dividedBy: 40),),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: screenWidth(context,dividedBy: 5),),
                          ResponseDrop(
                            resonseDropId: responseId,
                            getResponseDropId: (value){
                              setState(() {
                                responseDropId = value;
                              });
                              _responseId = value;
                            },
                          ),
                          SizedBox(width: screenWidth(context,dividedBy: 80),),
                          BuildRaisedButton(
                            backgroundColor: Constants.kitGradients[2],
                            buttonName: "Submit",
                            width: screenWidth(context, dividedBy: 15),
                            height: screenWidth(context, dividedBy: 50),
                            onPressed: (){
                              updateResponse();
                              updatePhoneStatus();

                            },
                          )

                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 50),
                      vertical: screenWidth(context, dividedBy: 200)),
                  child: Card(
                    child: Container(
                      height: screenHeight(context, dividedBy: 1),
                      width: 2,
                      color: Colors.black,
                    ),
                  ),
                ),
                QuestionAnswer()
              ],
            )
          : Container(
              child: Center(child: CircularProgressIndicator()),
            ),
    );
  }
}
