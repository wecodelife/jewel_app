import 'package:jewel_app/src/models/login_request_model.dart';
import 'package:jewel_app/src/models/state.dart';
import 'package:jewel_app/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> login({LoginRequestModel loginRequestModel}) =>
      userApiProvider.login(loginRequestModel: loginRequestModel);
}
