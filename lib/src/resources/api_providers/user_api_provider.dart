import 'package:jewel_app/src/bloc/user_bloc.dart';
import 'package:jewel_app/src/models/login_request_model.dart';
import 'package:jewel_app/src/models/login_response_model.dart';
import 'package:jewel_app/src/models/notification/notification_list_response.dart';
import 'package:jewel_app/src/models/state.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/utils.dart';

class UserApiProvider {
  Future<State> login({LoginRequestModel loginRequestModel}) async {
    final response = await ObjectFactory().apiClient.login(loginRequestModel);
    print(response.toString());
    if (response.statusCode == 200 || true) {
      return State<LoginResponseModel>.success(
          LoginResponseModel.fromJson(response.data));
    } else {
      return State<LoginResponseModel>.error(
          LoginResponseModel.fromJson(response.data));
    }
  }
}
