

class Urls{
// static final baseUrl = "https://telecaller.dev.wecodelife.com/";
  static final baseUrl = "https://jewel-telecaller.herokuapp.com/";
static final loginUrl =baseUrl+ "api/v1/login/";
// static final getCustomerUrl = baseUrl+"api/get-customer/";
  static final getCustomerUrl = baseUrl+"api/agent/get-agent/";
static final responseListUrl = baseUrl+"api/agent/contact-status/";
static final questionAnswerUrl = baseUrl+"api/get-questions/";
static final updateStatusUrl = baseUrl+"api/status/update/";
static final uploadFileUrl =baseUrl+ "api/files/";
static final answerQuestionUrl = baseUrl+"api/agent/add-answer/";
static final phoneNumUrl = baseUrl+"api/agent/phone-status/";
static final addRemark = baseUrl+"api/agent/add-agent-remarks/";
static final updateResponse = baseUrl+"api/agent/agent-status/update/";
static final updateStatus = baseUrl+"api/agent/agent-phone/status/update/";
static final addCustomer = baseUrl+"api/customer/add-customer/";
static final addQuestionRemark = baseUrl+"api/agent/add-answer/remarks/";

}