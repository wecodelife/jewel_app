import 'dart:async';

import 'package:jewel_app/src/models/login_request_model.dart';
import 'package:jewel_app/src/models/login_response_model.dart';
import 'package:jewel_app/src/models/state.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/src/utils/validators.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class UserBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<LoginResponseModel> _login =
      new StreamController<LoginResponseModel>.broadcast();

//stream controller is broadcasting the  details

  Stream<LoginResponseModel> get loginResponse => _login.stream;

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  login({LoginRequestModel loginRequestModel}) async {

    State state = await ObjectFactory().repository.login(
        loginRequestModel: LoginRequestModel(
            username: loginRequestModel.username,
            password: loginRequestModel.password));

    if (state is SuccessState) {
      _login.sink.add(state.value);
    } else if (state is ErrorState) {
      showToast("Please check user id and password");

      _login.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _login?.close();
  }
}

UserBloc userBlocSingle = UserBloc();
